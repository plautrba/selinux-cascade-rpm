#!/usr/bin/bash

if [ -z "$SNAPSHOT" ]; then
    SNAPSHOT=$(date +%Y%m%d%H%M%S)
fi

mkdir -p ~/rpmbuild/SOURCES

if [ ! -d cascade.git ]; then
    git clone https://github.com/dburgener/cascade.git cascade.git
else
    cd cascade.git
    git pull
    cd ..
fi

mkdir build

cp -v rust-selinux-cascade.spec build/
cp -r cascade.git build

cd build/cascade.git

cargo package
mv -v target/package/selinux-cascade-*.crate ~/rpmbuild/SOURCES

if [ ! -f  ~/rpmbuild/SOURCES/selinux-cascade-vendor.tar.gz ]; then

    if [ ! -d vendor ]; then
        cargo vendor
    fi

    for i in sexp clap_mangen clap_lex quick-xml is-terminal roff hermit-abi; do
        tar -r -f ~/rpmbuild/SOURCES/selinux-cascade-vendor.tar vendor/$i
    done

    tar -r -f ~/rpmbuild/SOURCES/selinux-cascade-vendor.tar vendor/windows*
    gzip ~/rpmbuild/SOURCES/selinux-cascade-vendor.tar
fi

cd ..

sed -i "s/-s snapshot/-s $SNAPSHOT/" rust-selinux-cascade.spec

rpmbuild -bs rust-selinux-cascade.spec

mock -r fedora-rawhide-x86_64 --no-clean --rebuild ~/rpmbuild/SRPMS/rust-selinux-cascade-*$SNAPSHOT*.src.rpm
